#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <iostream>
#include <stdexcept>
#include <functional>
#include <cstdlib>

class VulkanTriangle
{
public:
	void Run();

private:
	GLFWwindow* window;
	VkInstance instance;

	const std::vector<const char*> validationLayers = {
		"VK_LAYER_KHRONOS_validation"
	};

#ifdef NDEBUG
	const bool enableValidationLayers = false;
#else
	const bool enableValidationLayers = true;
#endif // NDEBUG

	bool CheckValidationLayersSupport();
	void InitWindow();
	void InitVulkan();
	void MainLoop();
	void Cleanup();
};